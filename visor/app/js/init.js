$(document).ready(function() {
  var sidebar = $('#sidebar').sidebar();
  window.sidebar = sidebar;
  $('.carousel').carousel();
  $('select').material_select();
  $('.modal').modal();
  // $('select').on('contentChanged', function() {
  //   // re-initialize (update)
  //   $(this).material_select();
  // });
  //$('ul.tabs').tabs();
});


window.mySubmitFunction = function (event) {
  var formulario = event.target;
  console.log('formulario', formulario);
  const payload = {
      "month": Number(formulario.month.value),
      "day": Number(formulario.day.value),
      "FFMC": Number(formulario.ffmc.value),
      "DMC": Number(formulario.dmc.value),
      "DC": Number(formulario.dc.value),
      "ISI": Number(formulario.isi.value),
      "temp": Number(formulario.temp.value),
      "RH": Number(formulario.rh.value),
      "wind": Number(formulario.wind.value),
      "rain": Number(formulario.rain.value)
  };
  window.sidebar.close();
  mapTools.buscar(mapTools.lastCoordinate, payload);
};